package com.epam.model;

import javax.persistence.*;

@Entity
@Table(name = "TV_series")
@DiscriminatorValue(value = "tv_series")
public class TVseries extends Movie {

	@Column
	private int seasonsNumber;

	@Column
	private int seriesNumber;

	public TVseries() {
	}

	public int getSeasonsNumber() {
		return seasonsNumber;
	}

	public void setSeasonsNumber(int seasonsNumber) {
		this.seasonsNumber = seasonsNumber;
	}

	public int getSeriesNumber() {
		return seriesNumber;
	}

	public void setSeriesNumber(int seriesNumber) {
		this.seriesNumber = seriesNumber;
	}
}

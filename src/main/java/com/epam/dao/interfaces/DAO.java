package com.epam.dao.interfaces;

import java.util.List;

public interface DAO<T> {

	void save(T o);
	T update(T o);
	T findById(int id);
	List<T> findAll();
	void remove(T o);

}
package com.epam.dao.impl;

import com.epam.dao.interfaces.DAO;
import com.epam.model.Movie;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

public class MovieDAOimpl implements DAO<Movie> {

	private static final String FIND_ALL = "SELECT m FROM Movie m WHERE TYPE(m) = Movie ";

	private EntityManager entityManager;

	public MovieDAOimpl() {
	}

	public MovieDAOimpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * Saves specified movie or TVseries to the database
	 *
	 * @param movie - movie to save
	 */
	@Override
	public void save(Movie movie) {
		if (entityManager.isJoinedToTransaction()) {
			entityManager.persist(movie);
		} else {
			EntityTransaction transaction = entityManager.getTransaction();
			transaction.begin();
			entityManager.persist(movie);
			transaction.commit();
		}
	}

	/**
	 * Updates specified movie or TV series in database
	 *
	 * @param movie - movie to update (detached object)
	 * @return - updated movie (managed object)
	 */
	@Override
	public Movie update(Movie movie) {
		EntityTransaction transaction;
		Movie updatedMovie;
		if (entityManager.isJoinedToTransaction()) {
			updatedMovie = entityManager.merge(movie);
		} else {
			transaction = entityManager.getTransaction();
			transaction.begin();
			updatedMovie = entityManager.merge(movie);
			transaction.commit();
		}
		return updatedMovie;
	}

	/**
	 * Finds movie by specified id in database
	 *
	 * @param id - id of movie search to
	 * @return - movie with specified id
	 */
	@Override
	public Movie findById(int id) {
		return entityManager.find(Movie.class, id);
	}

	/**
	 * Finds all movies in database
	 *
	 * @return - List of all movies in database
	 */
	@Override
	public List<Movie> findAll() {
		return entityManager
				.createQuery(FIND_ALL, Movie.class)
				.getResultList();
	}

	/**
	 * Removes specified movie in database
	 *
	 * @param movie - movie to remove
	 */
	@Override
	public void remove(Movie movie) {
		if (entityManager.isJoinedToTransaction()) {
			entityManager.remove(movie);
		} else {
			EntityTransaction transaction = entityManager.getTransaction();
			transaction.begin();
			entityManager.remove(movie);
			transaction.commit();
		}
	}
}

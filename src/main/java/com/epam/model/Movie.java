package com.epam.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "movies")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "type")
@DiscriminatorValue(value = "movie")
public class Movie {

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private int id;

	@Column
	private String title;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "director_id")
	private Director director;

	@ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinTable(
			name = "movie_genre",
			joinColumns = @JoinColumn(name = "movie_id"),
			inverseJoinColumns = @JoinColumn(name = "genre_id"))
	private List<Genre> genres = new ArrayList<>();

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "IMDB_info_id")
	private IMDBinfo IMDBinfo;

	@ElementCollection
	@CollectionTable(name = "awards", joinColumns = @JoinColumn(name = "movie_id"))
	private List<String> awards = new ArrayList<>();

	public Movie() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Director getDirector() {
		return director;
	}

	public void setDirector(Director director) {
		this.director = director;
	}

	public List<Genre> getGenres() {
		return genres;
	}

	public void setGenres(List<Genre> genres) {
		for (Genre genre : genres) {
			addGenre(genre);
		}
	}

	public void addGenre(Genre genre) {
		genre.getMovies().add(this);
		genres.add(genre);
	}

	public List<String> getAwards() {
		return awards;
	}

	public void setAwards(List<String> awards) {
		this.awards = awards;
	}

	public IMDBinfo getIMDBinfo() {
		return IMDBinfo;
	}

	public void setIMDBinfo(IMDBinfo IMDBinfo) {
		this.IMDBinfo = IMDBinfo;
	}
}

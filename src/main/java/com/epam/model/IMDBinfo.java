package com.epam.model;

import javax.persistence.*;

@Entity
@Table(name = "IMDB_info")
public class IMDBinfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column
	private String IMDBid;

	@Column
	private String language;

	@Column
	private String description;

	@Column
	private float rating;

	@Column
	private long budget;

	public IMDBinfo() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIMDBid() {
		return IMDBid;
	}

	public void setIMDBid(String IMDBid) {
		this.IMDBid = IMDBid;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public long getBudget() {
		return budget;
	}

	public void setBudget(long budget) {
		this.budget = budget;
	}
}

package com.epam.dao.interfaces;

import com.epam.model.Director;

import java.util.List;

public interface DirectorDAO extends DAO<Director> {

	List<Director> nativeFindDirectorsWIthNumberOfFilms(long numberOfFilms);
	List<Director> namedFindDirectorsWIthNumberOfFilms(long numberOfFilms);
	List<Director> JPQLFindDirectorsWIthNumberOfFilms(long numberOfFilms);
	List<Director> criteriaFindDirectorsWIthNumberOfFilms(long numberOfFilms);
}
package com.epam.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.LocalDate;

@Embeddable
public class LifeTime {

	@Column
	private LocalDate born;

	@Column
	private LocalDate died;

	public LifeTime() {
	}

	public LocalDate getBorn() {
		return born;
	}

	public void setBorn(LocalDate born) {
		this.born = born;
	}

	public LocalDate getDied() {
		return died;
	}

	public void setDied(LocalDate died) {
		this.died = died;
	}
}

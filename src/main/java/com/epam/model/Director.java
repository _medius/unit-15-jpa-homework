package com.epam.model;

import javax.persistence.*;

@NamedQuery(name =  "FindDirectorsWIthNumberOfFilms",
		query = "SELECT d FROM Movie m JOIN m.director d " +
				"GROUP BY d " +
				"HAVING COUNT(m.director)=:filmsnumber " +
				"ORDER BY d.lastName")
@Entity
@Table(name = "directors")
public class Director {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column
	private String firstName;

	@Column
	private String lastName;

	@Embedded
	private LifeTime lifeTime;

	@Enumerated(EnumType.STRING)
	@Column
	private Gender gender;

	public Director() {
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LifeTime getLifeTime() {
		return lifeTime;
	}

	public void setLifeTime(LifeTime lifeTime) {
		this.lifeTime = lifeTime;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}

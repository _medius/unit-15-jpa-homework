package com.epam.util;

import com.epam.model.*;

import java.time.LocalDate;
import java.time.Month;
import java.util.*;

/**
 * Class provides test data to fill database
 */
public class DataCreator {
	//vars to create director
	private String name;
	private String surname;
	private LocalDate born;
	private LocalDate died;
	private Gender gender;

	//vars to create genre
	private String genreTitle;
	private String genreDecription;

	//vars to create IMDBinfo
	private long budget;
	private String IMDBid;
	private String language;
	private String IMDBdescription;
	private float rating;

	//vars to create movie
	private Director movieDirector;
	private List<Genre> movieGenres;
	private IMDBinfo movieInfo;
	private String movieTitle;
	private List<String> awards;

	//additional vars to create TVseries
	private int seasonsNumber;
	private int seriesNumber;

	//collections of instances
	private Map<String, Director> directors = new HashMap<>();
	private Map<String, Genre> genres = new HashMap<>();
	private Map<String, IMDBinfo> infos = new HashMap<>();

	private List<Movie> movies = new ArrayList<>();
	private List<TVseries> TVs = new ArrayList<>();

	//instances for tests
	private Director testDirector = new Director();
	private Genre testGenre = new Genre();

	public DataCreator() {
		createDirectors();
		createGenres();
		createMovies();
		createTestDirector();
		createTestGenre();
	}

	public Director getTestDirector() {
		return testDirector;
	}

	public Genre getTestGenre() {
		return testGenre;
	}

	public List<Movie> getMovies() {
		return movies;
	}

	public List<TVseries> getTVs() {
		return TVs;
	}

	public Map<String, Director> getDirectors() {
		return directors;
	}

	public Map<String, Genre> getGenres() {
		return genres;
	}

	public Map<String, IMDBinfo> getInfos() {
		return infos;
	}

	private void createTestGenre() {
		genreTitle = "Comedy";
		genreDecription = "A genre of film in which the main emphasis is on humour.";

		testGenre.setTitle(genreTitle);
		testGenre.setDescription(genreDecription);
	}

	private void createTestDirector() {
		name = "Ron";
		surname = "Howard";
		born = LocalDate.of(1954, Month.MARCH, 1);
		died = null;
		gender = Gender.MALE;

		LifeTime lifeTime = new LifeTime();
		lifeTime.setBorn(born);
		lifeTime.setDied(died);

		testDirector.setFirstName(name);
		testDirector.setLastName(surname);
		testDirector.setGender(gender);
		testDirector.setLifeTime(lifeTime);
	}

	private void putDirector() {
		LifeTime lifeTime = new LifeTime();
		lifeTime.setBorn(born);
		lifeTime.setDied(died);

		Director director = new Director();
		director.setFirstName(name);
		director.setLastName(surname);
		director.setGender(gender);
		director.setLifeTime(lifeTime);

		directors.put(surname, director);
	}

	private void putGenre() {
		Genre genre = new Genre();
		genre.setTitle(genreTitle);
		genre.setDescription(genreDecription);

		genres.put(genreTitle, genre);
	}

	private void putIMDBinfo() {
		IMDBinfo info = new IMDBinfo();
		info.setBudget(budget);
		info.setDescription(IMDBdescription);
		info.setIMDBid(IMDBid);
		info.setLanguage(language);
		info.setRating(rating);

		infos.put(movieTitle, info);
	}

	private void putMovie() {
		Movie movie = new Movie();
		movie.setTitle(movieTitle);
		movie.setDirector(movieDirector);
		movie.setGenres(movieGenres);
		movie.setIMDBinfo(movieInfo);
		movie.setAwards(awards);

		movies.add(movie);
	}

	private void putTVseries() {
		TVseries tv = new TVseries();
		tv.setTitle(movieTitle);
		tv.setDirector(movieDirector);
		tv.setGenres(movieGenres);
		tv.setIMDBinfo(movieInfo);
		tv.setAwards(awards);
		tv.setSeasonsNumber(seasonsNumber);
		tv.setSeriesNumber(seriesNumber);

		TVs.add(tv);
	}

	private void createDirectors() {

		name = "Quentin";
		surname = "Tarantino";
		born = LocalDate.of(1963, Month.MARCH, 27);
		died = null;
		gender = Gender.MALE;
		putDirector();

		name = "Stanley";
		surname = "Kubrick";
		born = LocalDate.of(1928, Month.JULY, 26);
		died = LocalDate.of(1999, Month.MARCH, 7);
		gender = Gender.MALE;
		putDirector();

		name = "James";
		surname = "Cameron";
		born = LocalDate.of(1954, Month.AUGUST, 16);
		died = null;
		gender = Gender.MALE;
		putDirector();

		name = "Steven";
		surname = "Spielberg";
		born = LocalDate.of(1946, Month.DECEMBER, 18);
		died = null;
		gender = Gender.MALE;
		putDirector();
	}

	private void createGenres() {

		genreTitle = "Crime";
		genreDecription = "Films of this genre generally involve various aspects of crime and its detection.";
		putGenre();

		genreTitle = "Drama";
		genreDecription = "Genre of narrative fiction (or semi-fiction) intended to be more serious than humorous " +
				"in tone.";
		putGenre();

		genreTitle = "War";
		genreDecription = "Genre concerned with warfare, typically about naval, air, or land battles, with combat " +
				"scenes central to the drama.";
		putGenre();

		genreTitle = "Thriller";
		genreDecription = "Genre that involves suspense in the audience which is created by delaying what the " +
				"audience sees as inevitable, and is built through situations that are menacing or where escape " +
				"seems impossible.";
		putGenre();

		genreTitle = "Action";
		genreDecription = "Genre in which the protagonist or protagonists are thrust into a series of challenges " +
				"that typically include violence, extended fighting, physical feats, and frantic chases. ";
		putGenre();

		genreTitle = "Horror";
		genreDecription = "A film that seeks to elicit fear for entertainment purposes.";
		putGenre();

		genreTitle = "Mystery";
		genreDecription = "Focuses on the efforts of the detective, private investigator or amateur sleuth " +
				"to solve the mysterious circumstances of an issue by means of clues, investigation, and clever " +
				"deduction.";
		putGenre();

		genreTitle = "Biography";
		genreDecription = "Film that dramatizes the life of a non-fictional or historically-based person or people.";
		putGenre();

		genreTitle = "History";
		genreDecription = "Focuses on dramatic events in history.";
		putGenre();

		genreTitle = "Sci-Fi";
		genreDecription = "Genre that uses rtd-speculative, fictional science-based depictions of phenomena that are " +
				"not fully accepted by mainstream science.";
		putGenre();

		genreTitle = "Romance";
		genreDecription = "Focuses on passion, emotion, and the affectionate romantic involvement of the main " +
				"characters and the journey that their genuinely strong, true and pure romantic love takes them " +
				"through dating, courtship or marriage.";
		putGenre();
	}

	private void createMovies() {

		budget = 200000000;
		IMDBdescription = "A seventeen-year-old aristocrat falls in love with a kind but poor artist aboard the " +
				"luxurious, ill-fated R.M.S. Titanic.";
		IMDBid = "0120338";
		language = "English";
		rating = 7.8f;

		movieTitle = "Titanic";
		movieDirector = directors.get("Cameron");
		movieGenres = Arrays.asList(genres.get("Drama"), genres.get("Romance"));
		awards = Arrays.asList("Best picture 1998", "Best Director 1998", "Best cinematography 1998",
				"Best Art Direction-Set Decoration 1998","Best costume design 1998", "Best sound 1998",
				"Best film editing 1998", "Best sound effects 1998", "Best visual effects 1998",
				"Original song 1998", "Best music 1998");
		putIMDBinfo();
		movieInfo = infos.get(movieTitle);
		putMovie();


		budget = 6400000;
		IMDBdescription = "A seemingly indestructible robot is sent from 2029 to 1984 to assassinate a young " +
				"waitress, whose unborn son will lead humanity in a war against sentient machines, while a " +
				"human soldier from the same war is sent to protect her at all costs.";
		IMDBid = "0088247";
		language = "English";
		rating = 8.0f;

		movieTitle = "The Terminator";
		movieDirector = directors.get("Cameron");
		movieGenres = Arrays.asList(genres.get("Action"), genres.get("Sci-Fi"));
		awards = Arrays.asList("Best writing 1985", "Best make up 1985", "Best Science Fiction Film 1985");
		putIMDBinfo();
		movieInfo = infos.get(movieTitle);
		putMovie();


		budget = 102000000;
		IMDBdescription = "A cyborg, identical to the one who failed to kill Sarah Connor, must now protect her " +
				"teenage son, John Connor, from a more advanced and powerful cyborg.";
		IMDBid = "0103064";
		language = "English";
		rating = 8.5f;

		movieTitle = "Terminator 2: Judgment Day";
		movieDirector = directors.get("Cameron");
		movieGenres = Arrays.asList(genres.get("Action"), genres.get("Sci-Fi"));
		awards = Arrays.asList("Best sound 1992", "Best make up 1992", "Best sound effects 1992",
				"Best visual effects 1992");
		putIMDBinfo();
		movieInfo = infos.get(movieTitle);
		putMovie();


		budget = 52000000;
		IMDBdescription = "A seasoned FBI agent pursues Frank Abagnale Jr. who, before his 19th birthday, " +
				"successfully forged millions of dollars' worth of checks while posing as a Pan Am pilot, a " +
				"doctor, and a legal prosecutor.";
		IMDBid = "0264464";
		language = "English";
		rating = 8.1f;

		movieTitle = "Catch Me If You Can";
		movieDirector = directors.get("Spielberg");
		movieGenres = Arrays.asList(genres.get("Drama"), genres.get("Crime"), genres.get("Biography"));
		awards = Arrays.asList("Best Actor in a Supporting Role 2003", "Best Music 2003");
		putIMDBinfo();
		movieInfo = infos.get(movieTitle);
		putMovie();


		budget = 169000000;
		IMDBdescription = "Following the Normandy Landings, a group of U.S. soldiers go behind enemy lines to " +
				"retrieve a paratrooper whose brothers have been killed in action.";
		IMDBid = "0120815";
		language = "English";
		rating = 8.6f;

		movieTitle = "Saving Private Ryan";
		movieDirector = directors.get("Spielberg");
		movieGenres = Arrays.asList(genres.get("Drama"), genres.get("War"));
		awards = Arrays.asList("Best Director 1999", "Best Cinematography 1999", "Best Sound 1999",
				"Best Film Editing 1999", "Best Sound Effects 1999");
		putIMDBinfo();
		movieInfo = infos.get(movieTitle);
		putMovie();


		budget = 17000000;
		IMDBdescription = "A pragmatic U.S. Marine observes the dehumanizing effects the Vietnam War has on his " +
				"fellow recruits from their brutal boot camp training to the bloody street fighting in Hue.";
		IMDBid = "0093058";
		language = "English";
		rating = 8.3f;

		movieTitle = "Full Metal Jacket";
		movieDirector = directors.get("Kubrick");
		movieGenres = Arrays.asList(genres.get("Drama"), genres.get("War"));
		awards = Arrays.asList("Best Director 1988", "Best Supporting Actor 1988");
		putIMDBinfo();
		movieInfo = infos.get(movieTitle);
		putMovie();


		budget = 65000000;
		IMDBdescription = "A New York City doctor embarks on a harrowing, night-long odyssey of sexual and moral " +
				"discovery after his wife reveals a painful secret to him.";
		IMDBid = "0120663";
		language = "English";
		rating = 7.4f;

		movieTitle = "Eyes Wide Shut";
		movieDirector = directors.get("Kubrick");
		movieGenres = Arrays.asList(genres.get("Drama"), genres.get("Mystery"), genres.get("Thriller"));
		awards = Collections.singletonList("Best Film 1999");
		putIMDBinfo();
		movieInfo = infos.get(movieTitle);
		putMovie();


		budget = 19000000;
		IMDBdescription = "A family heads to an isolated hotel for the winter where a sinister presence influences " +
				"the father into violence, while his psychic son sees horrific forebodings from both past and future.";
		IMDBid = "0081505";
		language = "English";
		rating = 8.4f;

		movieTitle = "The Shining";
		movieDirector = directors.get("Kubrick");
		movieGenres = Arrays.asList(genres.get("Drama"), genres.get("Horror"));
		awards = Collections.singletonList("Best Supporting Actor 1981");
		putIMDBinfo();
		movieInfo = infos.get(movieTitle);
		putMovie();


		budget = 30000000;
		IMDBdescription = "After awakening from a four-year coma, a former assassin wreaks vengeance on the team of " +
				"assassins who betrayed her.";
		IMDBid = "0266697";
		language = "English";
		rating = 8.1f;

		movieTitle = "Kill Bill: Vol. 1";
		movieDirector = directors.get("Tarantino");
		movieGenres = Arrays.asList(genres.get("Action"), genres.get("Crime"), genres.get("Thriller"));
		awards = Arrays.asList("Best Actress 2004", "Best Action/Adventure/Thriller Film 2004");
		putIMDBinfo();
		movieInfo = infos.get(movieTitle);
		putMovie();


		budget = 1200000;
		IMDBdescription = "When a simple jewelry heist goes horribly wrong, the surviving criminals begin to " +
				"suspect that one of them is a police informant.";
		IMDBid = "0105236";
		language = "English";
		rating = 8.3f;

		movieTitle = "Reservoir Dogs";
		movieDirector = directors.get("Tarantino");
		movieGenres = Arrays.asList(genres.get("Drama"), genres.get("Crime"), genres.get("Thriller"));
		awards = Collections.singletonList("Best Original Screenplay 1992");
		putIMDBinfo();
		movieInfo = infos.get(movieTitle);
		putMovie();


		budget = 8000000;
		IMDBdescription = "The lives of two mob hitmen, a boxer, a gangster & his wife, and a pair of diner " +
				"bandits intertwine in four tales of violence and redemption.";
		IMDBid = "0110912";
		language = "English";
		rating = 8.9f;

		movieTitle = "Pulp Fiction";
		movieDirector = directors.get("Tarantino");
		movieGenres = Arrays.asList(genres.get("Drama"), genres.get("Crime"));
		awards = Collections.singletonList("Best Writing, Screenplay Written Directly for the Screen 1995");
		putIMDBinfo();
		movieInfo = infos.get(movieTitle);
		putMovie();


		budget = 125000000;
		IMDBdescription = "The story of Easy Company of the U.S. Army 101st Airborne Division, and their mission in " +
				"World War II Europe, from Operation Overlord, through V-J Day.";
		IMDBid = "0185906";
		language = "English";
		rating = 9.5f;

		movieTitle = "Band of Brothers";
		movieDirector = directors.get("Spielberg");
		movieGenres = Arrays.asList(genres.get("Drama"), genres.get("Action"), genres.get("History"));
		awards = Arrays.asList("Best Miniseries or Motion Picture Made for Television 2002",
				"Outstanding Miniseries 2002");
		seasonsNumber = 1;
		seriesNumber = 10;
		putIMDBinfo();
		movieInfo = infos.get(movieTitle);
		putTVseries();


		budget = 52000000;
		IMDBdescription = "Centers on the Shannons, an ordinary family from 2149 when the planet is dying, " +
				"who are transported back 85 million years to prehistoric Earth where they join Terra Nova, " +
				"a colony of humans with a second chance to build a civilization.";
		IMDBid = "1641349";
		language = "English";
		rating = 6.8f;

		movieTitle = "Terra Nova";
		movieDirector = directors.get("Spielberg");
		movieGenres = Arrays.asList(genres.get("Drama"), genres.get("Mystery"));
		awards = Collections.singletonList("Most Exciting New Series 2011");
		seasonsNumber = 1;
		seriesNumber = 13;
		putIMDBinfo();
		movieInfo = infos.get(movieTitle);
		putTVseries();
	}
}

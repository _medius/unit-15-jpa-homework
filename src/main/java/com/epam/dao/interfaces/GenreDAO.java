package com.epam.dao.interfaces;


import com.epam.model.Genre;

import java.util.List;

public interface GenreDAO extends DAO<Genre> {

	List<Genre> nativeFindGenresOnMaxBudgetOver(long budget);
	List<Genre> namedFindGenresOnMaxBudgetOver(long budget);
	List<Genre> JPQLFindGenresOnMaxBudgetOver(long budget);
	List<Genre> criteriaFindGenresOnMaxBudgetOver(long budget);
}

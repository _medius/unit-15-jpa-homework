package com.epam.dao.impl;

import com.epam.dao.interfaces.GenreDAO;
import com.epam.model.Genre;
import com.epam.model.Movie;

import javax.persistence.*;
import javax.persistence.criteria.*;
import java.util.List;

public class GenreDAOimpl implements GenreDAO {

	private static final String NATIVE_FIND_GENRES =    "SELECT g.id, g.title, g.description " +
			"FROM genres g " +
			"JOIN movie_genre mg ON g.id=mg.genre_id " +
			"JOIN movies m ON mg.movie_id=m.id " +
			"JOIN imdb_info i ON m.imdb_info_id=i.id " +
			"GROUP BY g.id, g.title, g.description " +
			"HAVING MAX(i.budget)>:budget " +
			"ORDER BY title";

	private static final String NAMED_FIND_GENRES =     "FindGenresOnMaxBudgetOver";

	private static final String JPQL_FIND_GENRES =      "SELECT g FROM Genre g JOIN g.movies m " +
			"GROUP BY g " +
			"HAVING MAX(m.IMDBinfo.budget)>:budget " +
			"ORDER BY g.title";

	private static final String BUDGET_PARAMETER =     "budget";

	private static final String FIND_ALL = "SELECT g FROM Genre g";

	private EntityManager entityManager;

	public GenreDAOimpl() {
	}

	public GenreDAOimpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * Finds genres which movies have a budget greater than the specified
	 * Native query
	 *
	 * @param budget - threshold value of budget
	 * @return - genre objects
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<Genre> nativeFindGenresOnMaxBudgetOver(long budget) {
		return entityManager
				.createNativeQuery(NATIVE_FIND_GENRES, Genre.class)
				.setParameter(BUDGET_PARAMETER, budget)
				.getResultList();
	}

	/**
	 * Finds genres which movies have a budget greater than the specified
	 * Named query
	 *
	 * @param budget - threshold value of budget
	 * @return - genre objects
	 */
	@Override
	public List<Genre> namedFindGenresOnMaxBudgetOver(long budget) {
		return entityManager
				.createNamedQuery(NAMED_FIND_GENRES, Genre.class)
				.setParameter(BUDGET_PARAMETER, budget)
				.getResultList();
	}

	/**
	 * Finds genres which movies have a budget greater than the specified
	 * JPQL query
	 *
	 * @param budget - threshold value of budget
	 * @return - genre objects
	 */
	@Override
	public List<Genre> JPQLFindGenresOnMaxBudgetOver(long budget) {
		return entityManager
				.createQuery(JPQL_FIND_GENRES, Genre.class)
				.setParameter(BUDGET_PARAMETER, budget)
				.getResultList();
	}

	/**
	 * Finds genres which movies have a budget greater than the specified
	 * Criteria query
	 *
	 * @param budget - threshold value of budget
	 * @return - genre objects
	 */
	@Override
	public List<Genre> criteriaFindGenresOnMaxBudgetOver(long budget) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Genre> genreQuery = criteriaBuilder.createQuery(Genre.class);
		Root<Genre> genre = genreQuery.from(Genre.class);
		Join<Genre, Movie> movie = genre.join("movies");

		genreQuery.select(genre)
				.groupBy(genre)
				.having(criteriaBuilder.gt(criteriaBuilder.max(movie.get("IMDBinfo").get("budget")), budget))
				.orderBy(criteriaBuilder.asc(genre.get("title")));

		return entityManager.createQuery(genreQuery).getResultList();
	}

	/**
	 * Saves specified genre to the database
	 *
	 * @param genre - genre to save
	 */
	@Override
	public void save(Genre genre) {
		if (entityManager.isJoinedToTransaction()) {
			entityManager.persist(genre);
		} else {
			EntityTransaction transaction = entityManager.getTransaction();
			transaction.begin();
			entityManager.persist(genre);
			transaction.commit();
		}
	}

	/**
	 * Updates specified genre in database
	 *
	 * @param genre - genre to update (detached object)
	 * @return - updated genre (managed object)
	 */
	@Override
	public Genre update(Genre genre) {
		Genre updatedGenre;
		if (entityManager.isJoinedToTransaction()) {
			updatedGenre = entityManager.merge(genre);
		} else {
			EntityTransaction transaction = entityManager.getTransaction();
			transaction.begin();
			updatedGenre = entityManager.merge(genre);
			transaction.commit();
		}
		return updatedGenre;
	}

	/**
	 * Finds genre by specified id in database
	 *
	 * @param id - id of genre to search
	 * @return - genre with specified id
	 */
	@Override
	public Genre findById(int id) {
		return entityManager.find(Genre.class, id);
	}

	/**
	 * Finds all genres in database
	 *
	 * @return - List of all genres in database
	 */
	@Override
	public List<Genre> findAll() {
		return entityManager
				.createQuery(FIND_ALL, Genre.class)
				.getResultList();
	}

	/**
	 * Removes specified genre in database
	 *
	 * @param genre - genre to remove
	 */
	@Override
	public void remove(Genre genre) {
		if (entityManager.isJoinedToTransaction()) {
			entityManager.remove(genre);
		} else {
			EntityTransaction transaction = entityManager.getTransaction();
			transaction.begin();
			entityManager.remove(genre);
			transaction.commit();
		}
	}

	/**
	 * Setter for entity manager
	 *
	 * @param entityManager - entity manager to set
	 */
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
}

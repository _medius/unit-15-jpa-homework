package com.epam.util;

import com.epam.dao.impl.*;
import com.epam.dao.interfaces.*;
import com.epam.model.*;

import javax.persistence.*;
import java.util.List;

public class App {

	private static final int REFERENCE_BUDGET = 60000000;
	private static final int NUMBER_OF_FILMS = 3;
	private static final int DIRECTOR_ID = 2;
	private static final int MOVIE_ID = 9;
	private static final int GENRE_ID = 5;

	private final EntityManagerFactory emFactory;
	private EntityManager entityManager;

	private DataCreator dataCreator = new DataCreator();

	public App() {
		this.emFactory = Persistence.createEntityManagerFactory("HibernateUnit");
	}

	public void start() {
		createDB();

		testDirectorDAO();
		testGenreDAO();
		testMovieDAO();

		closeEMFactory();
	}

	private void createDB() {
		DAO<Movie> dao = new MovieDAOimpl(getEntityManager());

		for (Movie movie : dataCreator.getMovies()) {
			dao.save(movie);
		}

		for (TVseries tv : dataCreator.getTVs()) {
			dao.save(tv);
		}

		getEntityManager().close();
	}

	private void testDirectorDAO() {
		DirectorDAO dao = new DirectorDAOimpl(getEntityManager());
		List<Director> directors;

		// Create operation
		dao.save(dataCreator.getTestDirector());
		System.out.println("\n**************DirectorDAO create**************");
		printDirectors(dao.findAll());

		// Read operations
		directors = dao.nativeFindDirectorsWIthNumberOfFilms(NUMBER_OF_FILMS);
		System.out.println("\n**************DirectorDAO native query**************");
		printDirectors(directors);

		directors = dao.JPQLFindDirectorsWIthNumberOfFilms(NUMBER_OF_FILMS);
		System.out.println("\n**************DirectorDAO JPQL query**************");
		printDirectors(directors);

		directors = dao.namedFindDirectorsWIthNumberOfFilms(NUMBER_OF_FILMS);
		System.out.println("\n**************DirectorDAO named query**************");
		printDirectors(directors);

		directors = dao.criteriaFindDirectorsWIthNumberOfFilms(NUMBER_OF_FILMS);
		System.out.println("\n**************DirectorDAO criteria query**************");
		printDirectors(directors);

		directors = dao.findAll();
		System.out.println("\n**************DirectorDAO find all**************");
		printDirectors(directors);

		Director directorForTest = dao.findById(DIRECTOR_ID);
		System.out.println("\n**************DirectorDAO find by id**************");
		System.out.println(directorForTest.getLastName());
		System.out.println();

		// Update operation
		directorForTest.setLastName("TestLastName");

		dao.update(directorForTest);

		System.out.println("\n**************DirectorDAO update**************");
		printDirectors(dao.findAll());

		// Delete operation
		dao.remove(directorForTest);
		System.out.println("\n**************DirectorDAO delete**************");
		printDirectors(dao.findAll());
	}

	private void testGenreDAO() {
		GenreDAO dao = new GenreDAOimpl(getEntityManager());
		List<Genre> genres;

		// Create operation
		dao.save(dataCreator.getTestGenre());
		System.out.println("\n**************GenreDAO create**************");
		printGenres(dao.findAll());

		// Read operations
		genres = dao.JPQLFindGenresOnMaxBudgetOver(REFERENCE_BUDGET);
		System.out.println("\n**************GenreDAO JPQL query**************");
		printGenres(genres);

		genres = dao.nativeFindGenresOnMaxBudgetOver(REFERENCE_BUDGET);
		System.out.println("\n**************GenreDAO native query**************");
		printGenres(genres);

		genres = dao.namedFindGenresOnMaxBudgetOver(REFERENCE_BUDGET);
		System.out.println("\n**************GenreDAO named query**************");
		printGenres(genres);

		genres = dao.criteriaFindGenresOnMaxBudgetOver(REFERENCE_BUDGET);
		System.out.println("\n**************GenreDAO criteria query**************");
		printGenres(genres);

		genres = dao.findAll();
		System.out.println("\n**************GenreDAO find all**************");
		printGenres(genres);

		Genre genreForTest = dao.findById(GENRE_ID);
		System.out.println("\n**************GenreDAO find by id**************");
		System.out.println(genreForTest.getTitle());
		System.out.println();

		// Update operation
		genreForTest.setTitle("TestTitle");

		dao.update(genreForTest);

		System.out.println("\n**************GenreDAO update**************");
		printGenres(dao.findAll());

		// Delete operation
		dao.remove(genreForTest);
		System.out.println("\n**************GenreDAO remove**************");
		printGenres(dao.findAll());
	}

	private void testMovieDAO() {
		DAO<Movie> dao = new MovieDAOimpl(getEntityManager());
		List<Movie> movies;

		// Create operation save() uses on createDB() method

		// Read operations
		movies = dao.findAll();
		System.out.println("\n**************MovieDAO find **************");
		printMovies(movies);

		Movie movieForTest = dao.findById(MOVIE_ID);
		System.out.println("\n**************MovieDAO find by id**************");
		System.out.println(movieForTest.getTitle());
		System.out.println();

		// Update operation
		movieForTest.setTitle("TestTitle");

		dao.update(movieForTest);

		System.out.println("\n**************MovieDAO update**************");
		printMovies(dao.findAll());

		// Delete operation
		dao.remove(movieForTest);
		System.out.println("\n**************MovieDAO delete**************");
		printMovies(dao.findAll());
	}

	private void printMovies(List<Movie> movies) {
		System.out.println();
		for (Movie movie : movies) {
			System.out.println(movie.getTitle());
		}
		System.out.println();
	}

	private void printDirectors(List<Director> directors) {
		System.out.println();
		for (Director director : directors) {
			System.out.println(director.getLastName());
		}
		System.out.println();
	}

	private void printGenres(List<Genre> genres) {
		System.out.println();
		for (Genre genre : genres) {
			System.out.println(genre.getTitle());
		}
		System.out.println();
	}

	private EntityManager getEntityManager() {
		if (entityManager == null || !entityManager.isOpen()) {
			entityManager = emFactory.createEntityManager();
		}
		return entityManager;
	}

	private void closeEMFactory() {
		emFactory.close();
	}
}

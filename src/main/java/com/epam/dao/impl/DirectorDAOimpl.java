package com.epam.dao.impl;

import com.epam.dao.interfaces.DirectorDAO;
import com.epam.model.Director;
import com.epam.model.Movie;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.*;
import java.util.List;

public class DirectorDAOimpl implements DirectorDAO {

	private static final String NATIVE_FIND_DIRECTORS = "SELECT * FROM( " +
			"SELECT d.id, d.firstname, d.lastname, d.gender, d.born, d.died " +
			"FROM directors d " +
			"JOIN movies m ON m.director_id=d.id " +
			") foo " +
			"GROUP BY id, firstname, lastname, gender, born, died " +
			"HAVING COUNT(id)=:filmsnumber " +
			"ORDER BY lastname";

	private static final String NAMED_FIND_DIRECTORS = "FindDirectorsWIthNumberOfFilms";

	private static final String JPQL_FIND_DIRECTORS = "SELECT d FROM Movie m JOIN m.director d " +
			"GROUP BY d " +
			"HAVING COUNT(m.director)=:filmsnumber " +
			"ORDER BY d.lastName";

	private static final String FILMS_NUMBER_PARAMETER = "filmsnumber";

	private static final String FIND_ALL = "SELECT d FROM Director d";

	private EntityManager entityManager;

	public DirectorDAOimpl() {
	}

	public DirectorDAOimpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * Find directors who have created specified number of films
	 * Native query
	 *
	 * @param numberOfFilms - number of films
	 * @return Director objects
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<Director> nativeFindDirectorsWIthNumberOfFilms(long numberOfFilms) {
		return entityManager
				.createNativeQuery(NATIVE_FIND_DIRECTORS, Director.class)
				.setParameter(FILMS_NUMBER_PARAMETER, numberOfFilms)
				.getResultList();
	}

	/**
	 * Find directors who have created specified number of films
	 * Named query
	 *
	 * @param numberOfFilms - number of films
	 * @return Director objects
	 */
	@Override
	public List<Director> namedFindDirectorsWIthNumberOfFilms(long numberOfFilms) {
		return entityManager
				.createNamedQuery(NAMED_FIND_DIRECTORS, Director.class)
				.setParameter(FILMS_NUMBER_PARAMETER, numberOfFilms)
				.getResultList();
	}

	/**
	 * Find directors who have created specified number of films
	 * JPQL query
	 *
	 * @param numberOfFilms - number of films
	 * @return Director objects
	 */
	@Override
	public List<Director> JPQLFindDirectorsWIthNumberOfFilms(long numberOfFilms) {
		return entityManager
				.createQuery(JPQL_FIND_DIRECTORS, Director.class)
				.setParameter(FILMS_NUMBER_PARAMETER, numberOfFilms)
				.getResultList();
	}

	/**
	 * Find directors who have created specified number of films
	 * Criteria query
	 *
	 * @param numberOfFilms - number of films
	 * @return Director objects
	 */
	@Override
	public List<Director> criteriaFindDirectorsWIthNumberOfFilms(long numberOfFilms) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Director> directorQuery = criteriaBuilder.createQuery(Director.class);
		Root<Movie> movie = directorQuery.from(Movie.class);
		Join<Movie, Director> director = movie.join("director");

		directorQuery.select(movie.get("director"))
				.groupBy(director)
				.having(criteriaBuilder.equal(criteriaBuilder.count(movie.get("director")), numberOfFilms))
				.orderBy(criteriaBuilder.asc(director.get("lastName")));

		return entityManager.createQuery(directorQuery).getResultList();
	}

	/**
	 * Saves specified director to the database
	 *
	 * @param director - director to save
	 */
	@Override
	public void save(Director director) {
		if (entityManager.isJoinedToTransaction()) {
			entityManager.persist(director);
		}
		EntityTransaction transaction = entityManager.getTransaction();
		transaction.begin();
		entityManager.persist(director);
		transaction.commit();
	}

	/**
	 * Updates specified director in the database
	 *
	 * @param director - director to update (detached object)
	 * @return - updated director (managed object)
	 */
	@Override
	public Director update(Director director) {
		Director updatedDirector;
		if (entityManager.isJoinedToTransaction()) {
			updatedDirector = entityManager.merge(director);
		} else {
			EntityTransaction transaction = entityManager.getTransaction();
			transaction.begin();
			updatedDirector = entityManager.merge(director);
			transaction.commit();
		}
		return updatedDirector;
	}

	/**
	 * Finds director by specified id in database
	 *
	 * @param id - id of director to search
	 * @return - director with specified id
	 */
	@Override
	public Director findById(int id) {
		return entityManager.find(Director.class, id);
	}

	/**
	 * Finds all directors in database
	 *
	 * @return - List or all directors
	 */
	@Override
	public List<Director> findAll() {
		return entityManager
				.createQuery(FIND_ALL, Director.class)
				.getResultList();
	}

	/**
	 * Removes specified director in database
	 *
	 * @param director - director to remove
	 */
	@Override
	public void remove(Director director) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Movie> findMoviesByDirector = criteriaBuilder.createQuery(Movie.class);
		Root<Movie> movie = findMoviesByDirector.from(Movie.class);

		findMoviesByDirector.select(movie).where(criteriaBuilder.equal(movie.get("director"), director));
		List<Movie> movies = entityManager.createQuery(findMoviesByDirector).getResultList();

		EntityTransaction transaction = entityManager.getTransaction();

		transaction.begin();
		for (Movie currentMovie : movies) {
			entityManager.remove(currentMovie);
		}
		entityManager.remove(director);
		transaction.commit();
	}

	/**
	 * Setter for entity manager
	 *
	 * @param entityManager - entity manager to set
	 */
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
}
